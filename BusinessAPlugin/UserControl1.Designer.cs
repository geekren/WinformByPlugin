﻿namespace BusinessAPlugin.Plugin
{
    partial class UserControl1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbNum = new System.Windows.Forms.TextBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.btnQuit = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgOutputParts = new System.Windows.Forms.DataGrid();
            this.btnReset = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbBoxCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectForm = new System.Windows.Forms.Button();
            this.tbOutFormID = new System.Windows.Forms.TextBox();
            this.lblOutFormID = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgOutputParts)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbNum
            // 
            this.tbNum.Location = new System.Drawing.Point(68, 74);
            this.tbNum.MaxLength = 50;
            this.tbNum.Name = "tbNum";
            this.tbNum.Size = new System.Drawing.Size(100, 21);
            this.tbNum.TabIndex = 12;
            // 
            // lblInfo
            // 
            this.lblInfo.BackColor = System.Drawing.SystemColors.Info;
            this.lblInfo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblInfo.ForeColor = System.Drawing.Color.Blue;
            this.lblInfo.Location = new System.Drawing.Point(2, 167);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(228, 60);
            this.lblInfo.TabIndex = 14;
            this.lblInfo.Text = "请选择出库单进行出库作业！";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "零件数";
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(160, 123);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(60, 20);
            this.btnQuit.TabIndex = 7;
            this.btnQuit.Text = "退出 ESC";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(13, 123);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(60, 20);
            this.btnSubmit.TabIndex = 6;
            this.btnSubmit.Text = "提交 F1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgOutputParts);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(232, 242);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "出库作业";
            // 
            // dgOutputParts
            // 
            this.dgOutputParts.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgOutputParts.DataMember = "";
            this.dgOutputParts.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgOutputParts.Location = new System.Drawing.Point(5, 7);
            this.dgOutputParts.Name = "dgOutputParts";
            this.dgOutputParts.Size = new System.Drawing.Size(228, 220);
            this.dgOutputParts.TabIndex = 0;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(89, 123);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(60, 20);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "重置 F2";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tbNum);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.lblInfo);
            this.tabPage1.Controls.Add(this.btnQuit);
            this.tabPage1.Controls.Add(this.btnSubmit);
            this.tabPage1.Controls.Add(this.btnReset);
            this.tabPage1.Controls.Add(this.tbBoxCode);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btnSelectForm);
            this.tabPage1.Controls.Add(this.tbOutFormID);
            this.tabPage1.Controls.Add(this.lblOutFormID);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(232, 242);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "出库";
            // 
            // tbBoxCode
            // 
            this.tbBoxCode.Location = new System.Drawing.Point(68, 40);
            this.tbBoxCode.MaxLength = 50;
            this.tbBoxCode.Name = "tbBoxCode";
            this.tbBoxCode.Size = new System.Drawing.Size(100, 21);
            this.tbBoxCode.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "箱条码";
            // 
            // btnSelectForm
            // 
            this.btnSelectForm.Location = new System.Drawing.Point(177, 8);
            this.btnSelectForm.Name = "btnSelectForm";
            this.btnSelectForm.Size = new System.Drawing.Size(50, 20);
            this.btnSelectForm.TabIndex = 2;
            this.btnSelectForm.Text = "选择";
            // 
            // tbOutFormID
            // 
            this.tbOutFormID.BackColor = System.Drawing.SystemColors.Info;
            this.tbOutFormID.Enabled = false;
            this.tbOutFormID.Location = new System.Drawing.Point(68, 8);
            this.tbOutFormID.Name = "tbOutFormID";
            this.tbOutFormID.Size = new System.Drawing.Size(100, 21);
            this.tbOutFormID.TabIndex = 1;
            // 
            // lblOutFormID
            // 
            this.lblOutFormID.Location = new System.Drawing.Point(13, 11);
            this.lblOutFormID.Name = "lblOutFormID";
            this.lblOutFormID.Size = new System.Drawing.Size(50, 20);
            this.lblOutFormID.TabIndex = 16;
            this.lblOutFormID.Text = "出库单";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 268);
            this.tabControl1.TabIndex = 1;
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(240, 268);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgOutputParts)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbNum;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGrid dgOutputParts;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox tbBoxCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectForm;
        private System.Windows.Forms.TextBox tbOutFormID;
        private System.Windows.Forms.Label lblOutFormID;
        private System.Windows.Forms.TabControl tabControl1;
    }
}
