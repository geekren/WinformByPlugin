﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginBase.ICommon
{
    public interface ILogger
    {
        ILogger Error(object sender, string message);

        ILogger Error(object sender, Exception ex);

        ILogger Error(object sender, string message, Exception ex);

        ILogger Error(object sender, string functionName, string message, Exception ex);

        ILogger Info(object sender, string msg);

        ILogger Trace(object sender, string msg);

        ILogger Log(object sender, string msg);
    }
}
