﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace PluginBase.ICommon
{
    public interface IGetPlugin
    {
        List<string> FindPlugin();

        object LoadObject(Assembly asm, string className, string interfacename, object[] param);

        List<string> DeleteInvalidPlungin(List<string> PlunginPath);

        List<Type> _delegateMethod();
    }
}
