﻿using Microsoft.CSharp;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.Net;
using System.Text;
using System.Web.Services.Description;

namespace CommonLibrary.InvokeWebService
{
    public class WebServiceHelp
    {
        public static object InvokeWebService(string url, string methodname, object[] args = null)
        {
            return WebServiceHelp.InvokeWebService(url, (string)null, methodname, args);
        }

        public static object InvokeWebService(string url, string classname, string methodname, object[] args = null)
        {
            string name = "EnterpriseServerBase.WebService.DynamicWebCalling";
            if (classname == null || classname == "")
                classname = WebServiceHelp.GetWsClassName(url);
            try
            {
                ServiceDescription serviceDescription = ServiceDescription.Read(new WebClient().OpenRead(url + "?WSDL"));
                ServiceDescriptionImporter descriptionImporter = new ServiceDescriptionImporter();
                descriptionImporter.AddServiceDescription(serviceDescription, "", "");
                CodeNamespace codeNamespace = new CodeNamespace(name);
                CodeCompileUnit codeCompileUnit = new CodeCompileUnit();
                codeCompileUnit.Namespaces.Add(codeNamespace);
                int num = (int)descriptionImporter.Import(codeNamespace, codeCompileUnit);
                CompilerResults compilerResults = new CSharpCodeProvider().CompileAssemblyFromDom(new CompilerParameters()
                {
                    GenerateExecutable = false,
                    GenerateInMemory = true,
                    ReferencedAssemblies = {
                    "System.dll",
                    "System.XML.dll",
                    "System.Web.Services.dll",
                    "System.Data.dll"
                  }
                }, codeCompileUnit);
                if (compilerResults.Errors.HasErrors)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (CompilerError error in (CollectionBase)compilerResults.Errors)
                    {
                        stringBuilder.Append(error.ToString());
                        stringBuilder.Append(Environment.NewLine);
                    }
                    throw new Exception(stringBuilder.ToString());
                }
                Type type = compilerResults.CompiledAssembly.GetType(name + "." + classname, true, true);
                object instance = Activator.CreateInstance(type);
                return type.GetMethod(methodname).Invoke(instance, args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.Message, new Exception(ex.InnerException.StackTrace));
            }
        }

        private static string GetWsClassName(string wsUrl)
        {
            string[] strArray = wsUrl.Split('/');
            return strArray[strArray.Length - 1].Split('.')[0];
        }
    }
}