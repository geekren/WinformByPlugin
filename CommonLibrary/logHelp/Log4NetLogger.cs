﻿using log4net;
using log4net.Core;
using System;
using System.Diagnostics;
using System.Reflection;

namespace CommonLibrary.logHelp
{
    public class Log4NetLogger : PluginBase.ICommon.ILogger
    {
        private static readonly ILog loginfo = LogManager.GetLogger("PDA.Logging.Info");
        private static readonly ILog logerror = LogManager.GetLogger("PDA.Logging.Error");
        private static readonly ILog logtrace = LogManager.GetLogger("PDA.Logging.Trace");
        public const int InfoOrError_Info = 1;
        public const int InfoOrError_Error = 2;

        [Obsolete]
        public PluginBase.ICommon.ILogger Error(object sender, string message)
        {
            this.Error(sender, string.Empty, message, (Exception)null);
            return (PluginBase.ICommon.ILogger)this;
        }

        [Obsolete]
        public PluginBase.ICommon.ILogger Error(object sender, Exception ex)
        {
            this.Error(sender, string.Empty, string.Empty, ex);
            return (PluginBase.ICommon.ILogger)this;
        }

        [Obsolete]
        public PluginBase.ICommon.ILogger Error(object sender, string message, Exception ex)
        {
            this.Error(sender, string.Empty, message, ex);
            return (PluginBase.ICommon.ILogger)this;
        }

        public PluginBase.ICommon.ILogger Error(object sender, string functionName, string message, Exception ex)
        {
            string str = string.Format("{0}|当前方法:{1} | 详信息:{2} | 详细信息:{3}", new object[4]
            {
        (object) DateTime.Now.ToString("yyyyMMddHHmmss"),
        (object) Log4NetLogger.GetReferralMethod(),
        (object) message,
        (object) ex
            });
            if (Log4NetLogger.logerror != null && Log4NetLogger.logerror.IsInfoEnabled)
            {
                LoggingEvent logEvent = new LoggingEvent(typeof(Log4NetLogger), Log4NetLogger.logerror.Logger.Repository, Log4NetLogger.logerror.Logger.Name, Level.Error, (object)str, ex);
                logEvent.Properties["catalog"] = (object)functionName;
                Log4NetLogger.logerror.Logger.Log(logEvent);
            }
            return (PluginBase.ICommon.ILogger)this;
        }

        public PluginBase.ICommon.ILogger Info(object sender, string msg)
        {
            if (Log4NetLogger.loginfo != null && Log4NetLogger.loginfo.IsInfoEnabled)
                Log4NetLogger.loginfo.Info((object)string.Format("{0}|当前方法:{1} | 信息:{2}", (object)DateTime.Now.ToString("yyyyMMddHHmmss"), (object)Log4NetLogger.GetReferralMethod(), (object)msg));
            return (PluginBase.ICommon.ILogger)this;
        }

        public PluginBase.ICommon.ILogger Trace(object sender, string msg)
        {
            if (Log4NetLogger.logtrace != null && Log4NetLogger.logtrace.IsInfoEnabled)
                Log4NetLogger.logtrace.Info((object)string.Format("{0}|当前方法:{1} | 信息:{2}", (object)DateTime.Now.ToString("yyyyMMddHHmmss"), (object)Log4NetLogger.GetReferralMethod(), (object)msg));
            return (PluginBase.ICommon.ILogger)this;
        }

        public PluginBase.ICommon.ILogger Log(object sender, string msg)
        {
            return this.Info(sender, msg);
        }

        public static ILog GetLogger(string loggerName)
        {
            return LogManager.GetLogger(loggerName);
        }

        private static string GetReferralMethod()
        {
            MethodBase method = new StackTrace(true).GetFrame(2).GetMethod();
            return string.Format("{0}.{1}", (object)method.DeclaringType, (object)method.Name);
        }
    }
}
