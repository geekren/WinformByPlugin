﻿using PluginBase.ICommon;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CommonLibrary.PluginsHelp
{
    public class GetPlugin : IGetPlugin
    {
        private static AppDomain appDomain;

        public static AppDomain AppDomain
        {
            get
            {
                return GetPlugin.appDomain;
            }
            set
            {
                GetPlugin.appDomain = value;
            }
        }

        public List<string> FindPlugin()
        {
            List<string> stringList = new List<string>();
            try
            {
                foreach (string file in Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins"), "*.Plugin.dll"))
                    stringList.Add(file);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return stringList;
        }

        public object LoadObject(Assembly asm, string className, string interfacename, object[] param)
        {
            try
            {
                Type type = asm.GetType(className);
                if (type == (Type)null || !type.IsClass || (!type.IsPublic || type.IsAbstract) || type.GetInterface(interfacename) == (Type)null)
                    return (object)null;
                return Activator.CreateInstance(type, param) ?? (object)null;
            }
            catch
            {
                return (object)null;
            }
        }

        public List<string> DeleteInvalidPlungin(List<string> PlunginPath)
        {
            string fullName = typeof(PluginBase.IPlugin.IPlugin).FullName;
            List<string> stringList = new List<string>();
            foreach (string path in PlunginPath)
            {
                try
                {
                    Assembly asm = Assembly.LoadFile(path);
                    foreach (Type exportedType in asm.GetExportedTypes())
                    {
                        if (this.LoadObject(asm, exportedType.FullName, fullName, (object[])null) != null)
                            stringList.Add(path);
                    }
                }
                catch
                {
                    throw new Exception(path + "不是有效插件");
                }
            }
            return stringList;
        }

        public List<Type> _delegateMethod()
        {
            List<Type> typeList = new List<Type>();
            try
            {
                IGetPlugin getPlugin = (IGetPlugin)new GetPlugin();
                List<string> plugin = getPlugin.FindPlugin();
                foreach (string path in getPlugin.DeleteInvalidPlungin(plugin))
                {
                    if (Path.GetFileNameWithoutExtension(path) != string.Empty)
                    {
                        foreach (Type exportedType in Assembly.LoadFile(path).GetExportedTypes())
                        {
                            if (exportedType.GetInterface("IPlugin") != (Type)null)
                                typeList.Add(exportedType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return typeList;
        }
    }
}