﻿using CommonLibrary.PluginsHelp;
using PluginBase.ICommon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;

namespace WinformByPlugin
{
    public partial class MainForm : Form
    {
        private static AppDomain appDomain;
        private delegate List<Type> AsyncMethodCaller();
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnLoadPluginForm_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.Plugintarget.Text.Trim()))
                return;
            new MainForm.AsyncMethodCaller(((IGetPlugin)new GetPlugin())._delegateMethod).BeginInvoke(new AsyncCallback(this.loadPlugin), 
                (object)null);
        }

        private void btnclean_Click(object sender, EventArgs e)
        {
            this.panel2.Controls.Clear();
        }
        public static AppDomain AppDomain
        {
            get
            {
                return MainForm.appDomain;
            }
            set
            {
                MainForm.appDomain = value;
            }
        }

        private void loadPlugin(IAsyncResult result)
        {
            Type type = ((MainForm.AsyncMethodCaller)((AsyncResult)result).AsyncDelegate).EndInvoke(result).Find((Predicate<Type>)(x => x.Name == this.Plugintarget.Text.Trim()));
            if (type != (Type)null)
            {
                UserControl userControl = (UserControl)Activator.CreateInstance(type);
                this.BeginInvoke(new EventHandler(delegate
                {
                    this.panel2.Controls.Clear();
                    this.panel2.Controls.Add((Control)userControl);
                    userControl.Dock = DockStyle.Fill;
                }));
            }
            else
            {
                int num = (int)MessageBox.Show("没有找到对应的窗体插件");
            }
        }
    }
}
