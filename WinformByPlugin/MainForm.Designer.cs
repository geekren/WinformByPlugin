﻿namespace WinformByPlugin
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnLoadPluginForm = new System.Windows.Forms.Button();
            this.btnclean = new System.Windows.Forms.Button();
            this.Plugintarget = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Plugintarget);
            this.panel1.Controls.Add(this.btnclean);
            this.panel1.Controls.Add(this.btnLoadPluginForm);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(487, 100);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(487, 440);
            this.panel2.TabIndex = 1;
            // 
            // btnLoadPluginForm
            // 
            this.btnLoadPluginForm.Location = new System.Drawing.Point(187, 31);
            this.btnLoadPluginForm.Name = "btnLoadPluginForm";
            this.btnLoadPluginForm.Size = new System.Drawing.Size(75, 23);
            this.btnLoadPluginForm.TabIndex = 0;
            this.btnLoadPluginForm.Text = "加载插件窗体";
            this.btnLoadPluginForm.UseVisualStyleBackColor = true;
            this.btnLoadPluginForm.Click += new System.EventHandler(this.btnLoadPluginForm_Click);
            // 
            // btnclean
            // 
            this.btnclean.Location = new System.Drawing.Point(298, 30);
            this.btnclean.Name = "btnclean";
            this.btnclean.Size = new System.Drawing.Size(75, 23);
            this.btnclean.TabIndex = 1;
            this.btnclean.Text = "清除窗体";
            this.btnclean.UseVisualStyleBackColor = true;
            this.btnclean.Click += new System.EventHandler(this.btnclean_Click);
            // 
            // Plugintarget
            // 
            this.Plugintarget.Location = new System.Drawing.Point(12, 31);
            this.Plugintarget.Name = "Plugintarget";
            this.Plugintarget.Size = new System.Drawing.Size(158, 21);
            this.Plugintarget.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 540);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnclean;
        private System.Windows.Forms.Button btnLoadPluginForm;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox Plugintarget;
    }
}

